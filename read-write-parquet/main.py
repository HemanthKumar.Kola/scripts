import pyarrow.parquet as pq
import pyarrow as pa
import pandas as pd

X = 5
Y = 5

ShardTable = [[0] * Y for _ in range(X)]

def init_shard_table():
    global ShardTable
    for i in range(X):
        for j in range(Y):
            ShardTable[i][j] = (i * Y) + j + 1

def hash_uuid_to_integer(uuid, k):
    final = 0
    for i in range(k):
        hex_byt = uuid[i]
        if '0' <= hex_byt <= '9':
            final += ord(hex_byt) - ord('0')
        else:
            final += 10 + ord(hex_byt) - ord('a')
    return final % k

def custom_function(str1, str2):
    return ShardTable[hash_uuid_to_integer(str1, X)][hash_uuid_to_integer(str2, Y)]

init_shard_table()

#print(custom_function("9f9f823", "f39f3992"))

#################################################

table = pq.read_table('updated1.parquet')

# print(table.schema) to know the schema

chunkedColumn1 = table['AIR_TIME'] #replace with column
strList1 = [str(val) for val in chunkedColumn1]

chunkedColumn2 = table['ARR_TIME'] #replace with column
strList2 = [str(val) for val in chunkedColumn1]

combined_list = [custom_function(val1, val2) for val1, val2 in zip(strList1, strList2)]

# Convert the combined list to a PyArrow array
combined_column = pa.array(combined_list)

# Add the new column to the table
new_table = table.append_column('new_column', combined_column)

# Write the new table with the new column to a new Parquet file
pq.write_table(new_table, 'updated1.parquet')